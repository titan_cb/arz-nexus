name             'arz-nexus'
maintainer       'ARZ Allgemeines Rechenzentrum GmbH'
maintainer_email 'christian.bitschnau@arz.at'
license          'Apache 2.0'
description      'Installs/Configures arz-nexus'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.3'

depends          'nexus'

